﻿using Xunit;
using LastCardModel.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LastCardModel.CardModels;

namespace LastCardModel.Units.Tests
{

    public class CardModelTest
    {
        [Fact()]
        public void CreateAllCartTest()
        {
            List<CardModel> list = new List<CardModel>();

            //Add all suit cards
            foreach (CardSuit suit in Enum.GetValues(typeof(CardSuit)))
                foreach (CardNumber number in Enum.GetValues(typeof(CardNumber)))
                    list.Add(new CardModel(number, suit));
            //Add jokers
            for (int i = 0; i < 2; i++)
                list.Add(new CardModel(false, true));

            //Add empty card
            list.Add(new CardModel(true, false));

            Assert.Equal(55, list.Count);
        }
        [Fact()]
        public void SumPointsTest()
        {
            List<CardModel> list = new List<CardModel>();

            //Add all suit cards
            foreach (CardSuit suit in Enum.GetValues(typeof(CardSuit)))
                foreach (CardNumber number in Enum.GetValues(typeof(CardNumber)))
                    list.Add(new CardModel(number, suit));
            //Add jokers
            for (int i = 0; i < 2; i++)
                list.Add(new CardModel(false, true));

            //Add empty card
            list.Add(new CardModel(true, false));

            int countPoint = list.Sum(x=>x.Point);
            Assert.Equal(400, countPoint);
        }
    }
}