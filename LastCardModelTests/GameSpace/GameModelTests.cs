﻿using Xunit;
using LastCardModel.GameSpace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastCardModel.GameSpace.Tests
{
    public class GameModelTests
    {
        [Fact()]
        public void GameModelTest()
        {
            GameModel model = new GameModel(5, 200);
            Assert.Equal(200, model.MaxPoints);
            Assert.Equal(5, model.ShiftCardCount);
            Assert.Equal(1, model.CountRounds);
        }

        [Fact()]
        public void DefinePlayerTest()
        {
            GameModel gameModel = new GameModel(5, 200);
            int randPL = gameModel.DefinePlayer();
            int randPL2 = gameModel.DefinePlayer();
            Assert.True(randPL > 0 && randPL < 3);
            Assert.True(randPL2 > 0 && randPL2 < 3);
            Assert.NotEqual(randPL, randPL2);
        }

        [Fact()]
        public void NewGameTest()
        {
            GameModel model = new GameModel(5, 200);
            model.DefinePlayer();
            model.NewGame();
            Assert.Equal(RoundStage.Distribution, model.Round.Stage);
            Assert.Equal(1, model.Round._RoundNumber);
        }

        [Fact()]
        public void CloneTest()
        {
            GameModel model = new GameModel(5, 200);
            model.DefinePlayer();
            model.NewGame();

            GameModel modelClone = (GameModel)model.Clone();
            model.MaxPoints = 300;
            Assert.NotEqual(model.MaxPoints, modelClone.MaxPoints);
            Assert.NotEqual(model, modelClone);
        }
    }
}