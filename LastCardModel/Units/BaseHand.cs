using LastCardModel.CardModels;
using System;
using System.Collections.Generic;

namespace LastCardModel.Units
{
    public abstract class BaseHand : ICloneable
    {
        public List<CardModel> CardsOnHand { get; set; }
        public CardModel TopCard;

        public BaseHand()
        {
            CardsOnHand = new List<CardModel> { };
        }
        public bool CheckWin() => CardsOnHand.Count == 0;

        internal void AddCard(CardModel tempCard)
        {
            if(!tempCard.Empty)
                CardsOnHand.Add(tempCard);
            TopCard = tempCard;
        }

        internal bool RemoveCard(CardModel card) => CardsOnHand.Remove(card);
        public abstract object Clone();

    }
}