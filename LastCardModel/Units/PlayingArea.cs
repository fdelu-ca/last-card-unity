using LastCardModel.CardModels;
using System;
using System.Collections.Generic;

namespace LastCardModel.Units
{
    public class PlayingArea : BaseHand
    {
        public PlayingArea() : base() { }
        public override object Clone()
        {
            PlayingArea clone = new PlayingArea();
            if (CardsOnHand?.Count > 0)
                foreach (var card in CardsOnHand)
                    clone.CardsOnHand.Add(card);
            return clone;
        }
    }
}