using System.Linq;

namespace LastCardModel.Units
{
    public abstract class BasePlayer : BaseHand
    {
        public BasePlayer() : base() { }
        public int Id { get; }
        public int PointsInRound => CardsOnHand.Sum(x => x.GetPoints());

        public BasePlayer(int id)
        {
            Id = id;
        }
    }
}