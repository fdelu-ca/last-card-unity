﻿using System;
using System.Collections.Generic;

namespace LastCardModel.Units
{
    public class Computer : BasePlayer
    {
        public Computer(int id) : base(id) { }
        public override object Clone()
        {
            Computer clone = new Computer(Id);
            if (CardsOnHand?.Count > 0)
                foreach (var card in CardsOnHand)
                    clone.CardsOnHand.Add(card);
            return clone;
        }

        public void ClearCards() => CardsOnHand.Clear();
    }
}