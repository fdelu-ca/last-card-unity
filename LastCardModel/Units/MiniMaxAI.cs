﻿using LastCardModel.CardModels;
using LastCardModel.GameSpace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastCardModel.Units
{
    public class MiniMaxAi
    {
        int _level;
        public int Level { get => _level; set { _level = value; } }

        public MiniMaxAi(int level)
        {
            _level = level;
        }

        public int CalcBestMove(GameModel board, out CardModel card)
        {
            int result = MiniMax(board, _level, int.MinValue, int.MaxValue, out CardModel resultCard);
            card = resultCard;
            return result;
        }
        public static int Heuristic(GameModel bState)
        {
            int result = 0;
            result -= bState.CurrentPlayer.CardsOnHand.Sum(x => x.GetPoints());
            result += bState.NextPlayer.CardsOnHand.Sum(x => x.GetPoints());
            return result;
        }

        public int MiniMax(GameModel board, int depth, int alpha, int beta, out CardModel card)
        {
            card = new CardModel();
            if (depth == 0)
                return Heuristic(board);

            int best = int.MinValue;

            //Формирование возможных ходов
            var moves = board.HaveMoves();
            if (moves.Count == 0)
                return Heuristic(board);

            //Проход по вариантам с расчётом 
            foreach (var move in moves)
            {
                //Создание доски
                GameModel childBoard = (GameModel)board.Clone();

                //Получение результата хода
                childBoard.Move(move);
                int value = MaxiMin(childBoard, depth - 1, alpha, beta, out CardModel outCard);

                //Определение наибольшего звена
                if (value > best)
                {
                    card = outCard;
                    best = value;
                }
                alpha = Math.Max(alpha, best);

                // Отсечение
                if (beta <= alpha)
                    break;
            }
            return best;
        }
        public int MaxiMin(GameModel board, int depth, int alpha, int beta, out CardModel card)
        {
            card = new CardModel();
            if (depth == 0)
                return Heuristic(board);

            int best = int.MaxValue;

            //Формирование возможных ходов
            var moves = board.HaveMoves();
            if (moves.Count == 0)
                return Heuristic(board);

            //Проход по вариантам с расчётом 
            foreach (var move in moves)
            {
                //Создание доски
                GameModel childBoard = (GameModel)board.Clone();

                //Получение результата хода
                childBoard.Move(move);
                int value = MiniMax(childBoard, depth - 1, alpha, beta, out CardModel outCard);

                //Определение наименьшего звена
                if (value < best)
                {
                    card = outCard;
                    best = value;
                }
                beta = Math.Min(beta, best);
                if (beta <= alpha)
                    break;
            }
            return best;
        }
    }
}
