using LastCardModel.CardModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LastCardModel.Units
{
    public class Bank : BaseHand
    {
        public Bank() : base() { }
        public void Shuffle() => CardsOnHand.Shuffle<CardModel>();

        public void GeneratePullCards()
        {
            CardsOnHand = new List<CardModel>();

            //Добавление всех карт по номерам и мастям
            foreach (CardNumber number in Enum.GetValues(typeof(CardNumber)).Cast<CardNumber>())
                foreach (CardSuit suit in Enum.GetValues(typeof(CardSuit)).Cast<CardSuit>())
                        CardsOnHand.Add(new CardModel(number, suit));
            
            //Добавление джокеров в банк
            CardsOnHand.Add(new CardModel(false, true));
            CardsOnHand.Add(new CardModel(false, true));
        }

        /// <summary>
        /// Перемещает карту выбранному игроку
        /// </summary>
        /// <param name="to">кому</param>
        /// <param name="card">возвращает полученную карту или пустую, если карты отсутствуют</param>
        /// <returns>Возвращает false, при отсутствии карт в руке</returns>
        public bool GiveCard(BaseHand to, out CardModel card)
        {
            bool result = false;
            card = new CardModel(true, false);//empty card

            if (CardsOnHand.Count > 0)
            {
                card = CardsOnHand.First();
                result = this.TransferCard(to, card);
            }
            return result;
        }

        public override object Clone()
        {

            Bank clone = new Bank();

            if (CardsOnHand?.Count > 0) 
                foreach (var card in CardsOnHand)
                    clone.CardsOnHand.Add(card);
            return clone;
        }
    }
}