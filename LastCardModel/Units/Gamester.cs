using System;

namespace LastCardModel.Units
{
    public class Gamester : BasePlayer
    {
        public Gamester(int id) : base(id) { }

        public override object Clone()
        {
            Gamester clone = new Gamester(Id);
            if(CardsOnHand?.Count > 0)
                foreach (var card in CardsOnHand)
                    clone.CardsOnHand.Add(card);
            return clone;
        }
        public void ClearCards() => CardsOnHand.Clear();
    }
}
