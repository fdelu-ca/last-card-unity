using LastCardModel.CardModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LastCardModel.Units
{
    public static class CardHelper //TODO Review
    {
        public static bool TransferCard(this BaseHand from, BaseHand to, CardModel card)
        {
            if (!from.CardsOnHand.Contains(card))
                return false;

            CardModel tempCard = card;
            from.RemoveCard(card);
            to.AddCard(tempCard);
            return true;
        }
       public static bool FitCard(CardModel playerCard, CardModel cardOnTop)
        {
            if (cardOnTop.FourMode)
            {
                if ((cardOnTop.Number +1) == playerCard.Number)
                    return true;
                else
                    return false;
            }
            if (playerCard.IsJoker)
                return true;
            if (playerCard.Number == CardNumber.Eight)
                return true;
            if (cardOnTop.Empty)
                return true;
            if (cardOnTop.Suit == playerCard.Suit || cardOnTop.Number == playerCard.Number)
                return true;

            return false;
        }
        public static bool TransferCard(this BaseHand from, BaseHand to, List<CardModel> cards)
        {
            foreach (var card in cards) //Проверка на наличие всех карт у отправителя
                if (!from.CardsOnHand.Contains(card))
                    return false;

            foreach (var card in cards)
            { 
                CardModel tempCard = card;
                from.CardsOnHand.Remove(card);
                to.CardsOnHand.Add(tempCard);
            }
            return true;
        }

        private static readonly Random random = new Random();
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        public static bool CardCheck(this CardModel lower, CardModel upper)
        {
            if (lower.Empty)
                return true;
            else if (lower.Suit == upper.Suit || lower.Number == upper.Number)
                return true;
            else
                return false;
        }
    }
}