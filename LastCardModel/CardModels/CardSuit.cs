namespace LastCardModel.CardModels
{
    public enum CardSuit
    {
        Spades,     //трефы
        Clubs,      //пики
        Diamonds,   //бубны
        Hearts,     // черви
    }
}
