using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LastCardModel.CardModels
{
    public struct CardModel
    {
        public CardNumber Number;
        public CardSuit Suit;
        public bool IsJoker;
        public bool Empty { get; }
        public bool FourMode { get; set; }
        public int PlayerId;
        public CardModel(CardNumber number, CardSuit suit)
        {
            Number = number;
            Suit = suit;
            Empty = false;
            IsJoker = false;
            FourMode = false;
            PlayerId = 0;
        }
        public CardModel(bool empty, bool isJoker):this(0,0)
        {
            Empty = empty;
            IsJoker = isJoker;
        }
        public int GetPoints()
        {
            int points = 0;
            if (IsJoker)
                points = 40;
            else if (Number == CardNumber.Eight)
                points = 25;
            else if (Number == CardNumber.Ace)
                points = 15;
            else if (Number == CardNumber.King || Number == CardNumber.Queen || Number == CardNumber.Jack || Number == CardNumber.Ten)
                points = 10;

            return points;
        }
    }
}
