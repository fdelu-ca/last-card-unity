﻿using LastCardModel.CardModels;
using LastCardModel.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastCardModel.GameSpace
{
    public class RoundModel : ICloneable //TODO Write
    {
        readonly int _shiftCardCount;
        readonly int _firstPlayerId;

        public readonly int _RoundNumber;
        public int CurrentMove; // Номер шага в текущей игре
        public RoundStage Stage { get; private set; }
        public BasePlayer CurrentPlayer { get; private set; }
        public BasePlayer NextPlayer => CurrentPlayer.Id == Gamester.Id ? Computer: Gamester;
        
        //Units
        public Gamester Gamester { get; private set; }
        public Computer Computer { get; private set; }
        public Bank Bank { get; private set; }
        public PlayingArea PlayingArea { get; private set; }

        public RoundModel(int gameId, int shiftCardCount, int firstId)
        {
            _RoundNumber = gameId;
            _shiftCardCount = shiftCardCount;
            _firstPlayerId = firstId;
            //Обнуление юнитов
            Gamester = new Gamester(1); //Добавление игрока
            Computer = new Computer(2); //Добавление компьютера
            PlayingArea = new PlayingArea();
            Bank = new Bank();

            Bank.GeneratePullCards();
            Bank.Shuffle(); //Перетасовка карт в банке
            Stage = RoundStage.Distribution;

            //Определение следующего игрока
            if (_firstPlayerId == Gamester.Id)
                CurrentPlayer = Gamester;
            else
                CurrentPlayer = Computer;
        }
        /// <summary>
        /// Раздача карт
        /// </summary>
        /// <returns>возвращает да, при этапе игры раздача и передаёт карты игрокам</returns>
        public bool CardDistribution()
        {
            if (Stage != RoundStage.Distribution)
                return false;

            //Передача карт к игрокам
            List<CardModel> cardList;
            cardList = Bank.CardsOnHand.Take(_shiftCardCount).ToList();
            Bank.TransferCard(Gamester, cardList);
            cardList = Bank.CardsOnHand.Take(_shiftCardCount).ToList();
            Bank.TransferCard(Computer, cardList);

            //Переход на стадию хода
            Stage = RoundStage.MovePlayer;
            return true;
        }

        /// <summary>
        /// Получает карту из колоды и передаёт её игроку
        /// </summary>
        /// <param name="playerId">Идентификатор игрока</param>
        /// <returns>Возвращает объект переданной карты</returns>
        public CardModel GiveCard(int playerId)
        {
            if (CurrentPlayer.Id != playerId && Stage == RoundStage.MovePlayer)
                return new CardModel(true,false); //Текущий игрок неверен

            bool result = Bank.GiveCard(CurrentPlayer, out CardModel card);
            if (!result)
            {
                while (PlayingArea.CardsOnHand.Count > 2)
                    PlayingArea.TransferCard(Bank, PlayingArea.CardsOnHand[0]);
                Bank.GiveCard(CurrentPlayer, out card);

            }

            Stage = RoundStage.PlayerAction;
            EndMove(playerId);

            return card;
        }

        /// <summary>
        /// Попытка хода заданной картой
        /// </summary>
        /// <returns></returns>
        public bool TryMoveCard(int playerId,CardModel card)
        {

            if (CurrentPlayer.Id != playerId)
                return false; //Текущий игрок неверен
            if(!CurrentPlayer.CardsOnHand.Contains(card)) // У игрока отсутствует данная карта
                return false;
            if (!CardHelper.FitCard(card,PlayingArea.TopCard)) //Карта не подходит
                return false;
            
            bool res = CurrentPlayer.TransferCard(PlayingArea, card);
            if(!res)
                return false;

            //Переход на выполнение эффектов от хода
            Stage = RoundStage.PlayerAction;
            EndMove(playerId);
            return true;
        }
       
        private void CardAction()
        {
            CardModel top = PlayingArea.TopCard;
            if (top.Empty)
                return;
            if (top.IsJoker)
            {
                Stage = RoundStage.SelectCard;
                return;
            }
            if(top.FourMode) // положить большую карту или взять кадр на номер карты
                if (top.PlayerId == CurrentPlayer.Id)
                    return;
                else
                {
                    List<CardModel> cardList;
                    cardList = Bank.CardsOnHand.Take((int)top.Number).ToList();
                    Bank.TransferCard(CurrentPlayer, cardList);
                }

            switch (top.Number)
            {
                case CardNumber.Two:    // Взять карту из запаса и пропустить ход
                    CurrentPlayer = NextPlayer;
                    Bank.GiveCard(CurrentPlayer, out CardModel card);
                    CurrentMove++;
                    break;
                case CardNumber.Three:  // Игрок ходит второй картой
                    CurrentPlayer = NextPlayer;
                    CurrentMove--;
                    break;
                case CardNumber.Four:   // Активирует режим 4,5,6,7,8
                    top.FourMode = true;
                    top.PlayerId = CurrentPlayer.Id;
                    break;
                case CardNumber.Eight:  // Объявить новую масть
                    Stage = RoundStage.SelectSuit;
                    break;
                case CardNumber.Jack:   // Противник пропускает ход
                    CurrentPlayer = NextPlayer;
                    break;
                //case CardNumber.Ace:    // Меняет направление
                //    break;
                default:break;
            }
        }
        public bool EndMove(int playerId)
        {
            if (CurrentPlayer.Id != playerId || Stage != RoundStage.PlayerAction)
                return false; // Идентификатор игрока не соответствует или стадия не та
            if(Stage == RoundStage.MovePlayer)
                return false; //Игрок не взял карту

            if (CurrentPlayer.CheckWin())
                Stage = RoundStage.Finish;
            else
            {
                CardAction();
                CurrentPlayer = NextPlayer;
                Stage = RoundStage.MovePlayer; 
            }
            CurrentMove++;
            return true;
        }

        public object Clone()
        {
            RoundModel clone = (RoundModel)MemberwiseClone();
            Gamester = (Gamester)this.Gamester.Clone();
            Computer = (Computer)this.Computer.Clone();
            Bank = (Bank)this.Bank.Clone();
            PlayingArea = (PlayingArea)this.PlayingArea.Clone();
            CurrentPlayer = this.CurrentPlayer.Equals(this.Gamester) ? Gamester : Computer;
            return clone;
        }
    }
}