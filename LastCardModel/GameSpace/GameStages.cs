namespace LastCardModel.GameSpace
{
    public enum RoundStage //TODO Review
    {
        PlayerAction,       // Действие игрока
        PlayerChange,       // Смена игрока
        CheckCard,          // Проверка карт
        SelectSuit,         // Выбор масти (ход 8)
        SelectCard,         // Выбор карты (ход джокера)

        Distribution,       // Раздача карт
        MovePlayer,         // Ход игрока
        Finish,             // Раунд завершён
    }
}
