using LastCardModel.CardModels;
using LastCardModel.Units;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LastCardModel.GameSpace
{
    public class GameModel : ICloneable //TODO Review
    {
        public RoundModel Round;
        int firstPlayerId;

        public int CountRounds { get; private set; } // Порядковый номер игры
        public int ShiftCardCount; // Количество карт при раздаче
        public int MaxPoints;     // Лимит в количестве очков
        public int GamesterPoints { get; private set; }
        public int ComputerPoints { get;private set; }
        public int Winner { get;private set; }
        public GameModel(int shiftCard, int maxPoints)
        {
            if (4 > shiftCard || shiftCard > 8)
                throw new ArgumentException("Ошибка выбора количества карт [от 4 до 8].");
            if (10 > maxPoints || maxPoints > 1000)
                throw new ArgumentException("Ошибка выбора лимита очков [от 10 до 1000].");

            firstPlayerId = 0;
            Winner = 0;
            ShiftCardCount = shiftCard;
            MaxPoints = maxPoints;
            CountRounds = 1;
        }

        public int DefinePlayer()
        {
            if (firstPlayerId == 0) //Выбор случайного игрока
            {
                firstPlayerId = new Random().Next(1, 2);
                Round = new RoundModel(CountRounds, ShiftCardCount, firstPlayerId);
            }
            else
                firstPlayerId = firstPlayerId == Round.Gamester.Id ? Round.Computer.Id : Round.Gamester.Id;
            return firstPlayerId;
        }
        public BasePlayer GetPlayer(int id)
        {
            if (id == 1)
                return Round.Gamester;
            else if(id == 2)
                return Round.Computer;
            else
                throw new ArgumentException($"Номер первого игрока равен {id}, выходит за границы [1-2]");
        }

       public void NewGame()
        {
            if (GamesterPoints >= MaxPoints)
                Winner = Round.Computer.Id;
            else if (ComputerPoints >= MaxPoints)
                Winner = Round.Gamester.Id;
            else
            {

                int firstPLayer = DefinePlayer();//Определение первого игрока
                RoundModel currRound = new RoundModel(CountRounds, ShiftCardCount, firstPLayer);
                Round = currRound;
            }
        }
        public bool EndRound()
        {
            if(Round.Stage != RoundStage.Finish || CountRounds != Round._RoundNumber)
                return false;
            //Расчёт очков для игроков
            GamesterPoints += Round.Gamester.PointsInRound;
            ComputerPoints += Round.Computer.PointsInRound;

            CountRounds++;
            return true;
        }
        public object Clone()
        {
            GameModel clone = (GameModel)MemberwiseClone();
            clone.Round = (RoundModel)this.Round.Clone();
            return clone;
        }


        #region Old

        private static GameModel instance;
        public static GameModel GetInstance(int shiftCard, int maxPoints)
        {
            if (instance == null)
                instance = new GameModel(shiftCard, maxPoints);
            return instance;
        }

        //TODO Review Moves
        /*
        public List<CardModel> HaveMoves()
        {
            List<CardModel> resultMoves = new List<CardModel>();
            resultMoves.Add(new CardModel(true,false));

            foreach (var card in CurrentPlayer.CardsOnHand)
                if (PlayingArea.TopCard.CardCheck(card))
                    resultMoves.Add(card);

            return resultMoves;
        }


        public bool Move(CardModel card) 
        {
            //Проверка пустого списка, игрок берёт карту и пропускает ход
            if (card.Empty)
            {
                Bank.GiveCards(CurrentPlayer, 1);
                CurrentPlayer = NextPlayer;
                return true;
            }
            //Проверка карты
            if (!PlayingArea.TopCard.CardCheck(card))
                return false;

            CurrentPlayer.TransferCard(PlayingArea, card);
            switch (card.Number)
            {
                case CardNumber.Two: //2 карты
                    Bank.GiveCards(NextPlayer, 2);
                    break;
                case CardNumber.Five://5 карт
                    Bank.GiveCards(NextPlayer, 5);
                    break;
                case CardNumber.Ace: //Пропуск хода
                    CurrentPlayer = NextPlayer;
                    break;
                default:
                    break;
            }
            CurrentPlayer = NextPlayer;

            return true;
        }
        */
        #endregion
    }
}
